let varNum = Number(prompt("Enter Number"));

console.log("The number you provided is " + varNum);

for(varNum; varNum !== 0; varNum--){

	if(varNum <= 50){

		console.log("The current value is at " + varNum + ". Terminating the loop.")
		break;

	};

	if(varNum % 10 === 0){
		
		console.log("The number is divisible by 10. Skipping the number.");

		continue;

	};

	if(varNum % 5 === 0){
		
		console.log(varNum);
	};

};
